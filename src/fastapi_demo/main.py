from fastapi import FastAPI
from typing import Union

app = FastAPI()


@app.get('/')
def get_root():
    return "hello world"


@app.get('/items/{item_id}')
def get_item(item_id: int):
    return {"item_id": item_id}


@app.get('/goods')
def get_goods():
    goods = [i for i in range(100)]
    return {"goods": goods}


if __name__ == '__main__':
    print("hello world")
